import React from 'react';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import ModalWrapper from '../../app/common/modals/ModalWrapper';
import TextInput from '../../app/common/form/TextInput';
import { Button } from 'semantic-ui-react';
import { useDispatch } from 'react-redux';
import { signinUser } from './authReducer';
import { closeModal } from '../../app/common/modals/modalReducer';

const LoginForm = () => {
  const dispatch = useDispatch();
  return (
    <ModalWrapper size="mini" header="Sign in to Re-vents">
      <Formik
        initialValues={{ email: '', password: '' }}
        validationSchema={Yup.object({
          email: Yup.string().email().required(),
          password: Yup.string().min(4).max(12).required(),
        })}
        onSubmit={(values, { setSubmitting }) => {
          dispatch(signinUser({ email: values?.email }));
          setSubmitting(false);
          dispatch(closeModal());
        }}
      >
        {({ isSubmitting, isValid, dirty }) => (
          <Form className="ui form">
            <TextInput name="email" placeholder="Email Address" />
            <TextInput
              name="password"
              placeholder="Password"
              type="password"
            />
            <Button
              loading={isSubmitting}
              disable={!isValid || !dirty || isSubmitting}
              fluid
              size="large"
              color="teal"
              content="Login"
            />
          </Form>
        )}
      </Formik>
    </ModalWrapper>
  );
};

export default LoginForm;
