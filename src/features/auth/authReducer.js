export const SIGN_IN_USER = 'SIGN_IN_USER';
export const SIGN_OUT_USER = 'SIGN_OUT_USER';

export const signinUser = payload => {
  return {
    type: SIGN_IN_USER,
    payload,
  };
};
export const signoutUser = payload => {
  return {
    type: SIGN_OUT_USER,
    payload,
  };
};

const initialState = {
  authenticated: false,
  user: null,
};

const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SIGN_IN_USER:
      return {
        ...state,
        authenticated: true,
        user: {
          email: payload?.email,
          photoURL: '/assets/user.png',
        },
      };
    case SIGN_OUT_USER:
      return {
        ...state,
        authenticated: false,
        user: null,
      };
    default:
      return state;
  }
};

export default authReducer;
