import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button } from 'semantic-ui-react';
import { openModal } from '../../app/common/modals/modalReducer';
import { decrement, increment } from './testReducer';

const Sanbox = () => {
  const dispatch = useDispatch();
  const { data } = useSelector(state => state.test);

  return (
    <>
      <h1>The data is: {data}</h1>
      <Button
        onClick={() => dispatch(increment(10))}
        content="increment"
        color="green"
      />
      <Button
        onClick={() => dispatch(decrement(1))}
        content="decrement"
        color="red"
      />
      <Button
        onClick={() =>
          dispatch(
            openModal({
              modalType: 'TestModal',
              modalProps: { data },
            }),
          )
        }
        content="Open Modal"
        color="teal"
      />
    </>
  );
};

export default Sanbox;
