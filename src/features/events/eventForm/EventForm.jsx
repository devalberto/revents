import React from 'react';
import cuid from 'cuid';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, Header, Segment } from 'semantic-ui-react';
import { createEvent, updateEvent } from '../eventActions';
import TextInput from '../../../app/common/form/TextInput';
import TextArea from '../../../app/common/form/TextArea';
import SelectInput from '../../../app/common/form/SelectInput';
import { categoryOptions } from '../../../app/api/categoryOptions';
import Date from '../../../app/common/form/Date';

const EventForm = ({ match, history }) => {
  const selectedEvent = useSelector(state =>
    state.events.events.find(event => event.id === match.params.id),
  );
  const dispatch = useDispatch();
  const initialValues = selectedEvent ?? {
    title: '',
    category: '',
    description: '',
    city: '',
    venue: '',
    date: '',
  };

  const validationSchema = Yup.object({
    title: Yup.string().required('You must provide a tittle'),
    category: Yup.string().required('You must provide a category'),
    description: Yup.string().required(),
    city: Yup.string().required(),
    venue: Yup.string().required(),
    date: Yup.string().required(),
  });

  return (
    <Segment clearing>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => {
          console.log(values);
          selectedEvent
            ? dispatch(updateEvent({ ...selectedEvent, ...values }))
            : dispatch(
                createEvent({
                  ...values,
                  id: cuid(),
                  hostedBy: 'Bob',
                  attendees: [],
                  hostPhotoURL: '/assets/user.png',
                }),
              );
          history.push('/events');
        }}
      >
        {({ isSubmiting, dirty, isValid }) => (
          <Form className="ui form">
            <Header sub color="teal" content="Event details" />
            <TextInput placeholder="Event title" name="title" />
            <SelectInput
              placeholder="Category"
              name="category"
              options={categoryOptions}
            />
            <TextArea
              placeholder="Description"
              name="description"
              rows={3}
            />
            <Header sub color="teal" content="Event location" />
            <TextInput placeholder="City" name="city" />
            <TextInput placeholder="Venue" name="venue" />
            <Date
              placeholderText="Date"
              name="date"
              timeFormat="HH:mm"
              showTimeSelect
              timeCaption="time"
              dateFormat="MMMM d, yyyy h:mm a"
            />
            <Button
              loading={isSubmiting}
              disabled={!isValid || !dirty || isSubmiting}
              type="submit"
              floated="right"
              positive
              content="Submit"
            />
            <Button
              disabled={isSubmiting}
              type="submit"
              as={Link}
              to={'/events'}
              floated="right"
              content="Cancel"
            />
          </Form>
        )}
      </Formik>
    </Segment>
  );
};

export default EventForm;
