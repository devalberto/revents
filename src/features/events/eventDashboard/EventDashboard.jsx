import React from 'react';
import { useSelector } from 'react-redux';
import { Grid } from 'semantic-ui-react';

import EventList from './EventList';

const EventDashboard = () => {
  const { events } = useSelector(state => state.events);
  return (
    <Grid>
      <Grid.Column width={10}>
        <EventList events={events} />
      </Grid.Column>
      <Grid.Column width={6}>
        <h3>Filter</h3>
      </Grid.Column>
    </Grid>
  );
};

export default EventDashboard;
