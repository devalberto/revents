import React from 'react';
import EventListItem from './EventListItem';

const EventList = ({ events, selectEvent, deleteEvent }) => {
  return (
    <>
      {events.map(event => {
        return <EventListItem key={event.id} event={event} />;
      })}
    </>
  );
};

export default EventList;
