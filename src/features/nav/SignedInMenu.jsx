import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Dropdown, Image, Menu } from 'semantic-ui-react';
import { signoutUser } from '../auth/authReducer';

const SignedInMenu = () => {
  const dispatch = useDispatch();
  const { user } = useSelector(state => state.auth);
  const history = useHistory();
  return (
    <Menu.Item position="right">
      <Image avatar spaced="right" src={user?.photoURL} />
      <Dropdown pointing="top left" text={user?.email}>
        <Dropdown.Menu>
          <Dropdown.Item
            as={Link}
            to="/createEvent"
            text="Create Event"
            icon="plus"
          />
          <Dropdown.Item text="My Profile" icon="user" />
          <Dropdown.Item
            onClick={() => {
              dispatch(signoutUser());
              history.push('/');
            }}
            text="Sign out"
            icon="power"
          />
        </Dropdown.Menu>
      </Dropdown>
    </Menu.Item>
  );
};

export default SignedInMenu;
