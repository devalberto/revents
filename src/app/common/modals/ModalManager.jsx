import React from 'react';
import { useSelector } from 'react-redux';
import TestModal from '../../../features/sandbox/TestModal';
import LoginForm from '../../../features/auth/LoginForm';

const ModalManager = () => {
  const modalLookup = { TestModal, LoginForm };
  const currentModal = useSelector(state => state.modals);

  if (currentModal) {
    const { modalType, modalProps } = currentModal;
    const ModalComponent = modalLookup[modalType];
    return (
      <span>
        <ModalComponent {...modalProps} />
      </span>
    );
  }

  return null;
};

export default ModalManager;
