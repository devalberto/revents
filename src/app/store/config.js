import { combineReducers, createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import authReducer from '../../features/auth/authReducer';
import eventReducer from '../../features/events/eventReducer';
import testReducer from '../../features/sandbox/testReducer';
import modalReducer from '../common/modals/modalReducer';

const configStore = () => {
  return createStore(
    combineReducers({
      test: testReducer,
      events: eventReducer,
      modals: modalReducer,
      auth: authReducer,
    }),
    devToolsEnhancer(),
  );
};

export default configStore;
